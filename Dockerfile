FROM ubuntu:16.04
ENV PYTHONUNBUFFERED 1



RUN mkdir /code
COPY  req2018.txt /code
WORKDIR /code


RUN apt-get -y dist-upgrade
RUN apt-get update

RUN apt-get install -y python3-pip
RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install -y python

RUN pip3 install --upgrade pip
RUN apt-get install -y libcups2-dev
RUN apt-get install -y apt-utils
RUN apt-get install -y build-essential
RUN apt-get update --fix-missing
RUN apt-get install -y libssl-dev
RUN apt-get install -y libffi-dev
RUN apt-get install -y python-dev
RUN pip3 install cryptography
RUN pip3 install -r  req2018.txt

RUN apt-get install -y git
RUN pip3 install -U scikit-learn
RUN pip3 install --upgrade pip



RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:mc3man/xerus-media
RUN apt-get update
RUN apt-get install -y ffmpeg --fix-missing

RUN apt-get install -y python3-tk


ADD . /code/
