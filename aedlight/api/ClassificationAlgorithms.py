import numpy as np
import json
import sys
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import SGDClassifier
from aedlight.settings import ROOT_URL, API_URL, AED_URL
from sklearn.externals import joblib
import os
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
from sklearn.model_selection import learning_curve


import wavio, subprocess,librosa


class ClassificationAlgorithms():
    def classify(id, X, event, files):
        print("In ClassificationAlgorithms.Classify", event)
        toreturn=[]
        try:
            curfilePath = os.path.abspath(__file__)
            curDir = os.path.abspath(os.path.join(curfilePath, os.pardir))
            curDir = os.path.abspath(os.path.join(curDir, os.pardir))
            curDir = os.path.abspath(os.path.join(curDir, os.pardir))
            print("********************************************************")
            

            id=curDir+"/models/"+id+".sav"
            print("curDir", curDir)
            clf = joblib.load(id)
            y=clf.predict_proba(X)
            
            print("y is: ", y, "and the shape of X is ", X.shape)
            reply=ClassificationAlgorithms.formOutput(clf,y,files)
            # print(json.dumps(reply,indent=4))
        except Exception as ex:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print("Exception ", str(ex),exc_type, fname, exc_tb.tb_lineno)

        # print(json.dumps(reply,indent=4))
        for item in reply["result"]:
            print(item, type(item))
            if item['event']==0:
                item['event']="False"
            else:
                item['event']=event
        print(json.dumps(reply,indent=4))
        return reply

    def formOutput(clf,y,files):
        response=[]

        for idx in range(0, len(y)):
            count=0
            for lbl in clf.classes_.tolist():
                # print(lbl, y[0])
                item={}
                item["event"]=lbl
                item["probability"]=y[idx][count]
                item["file"]= files["segmentName"]
                # item["file"]= files[idx]
                count+=1
                response.append(item)
        reply={'result':response}

        # print(json.dumps(reply,indent=4))
        return reply

    def extract_feature(file_name):
        try:
            print("In extract filename",file_name)
            X, sample_rate = librosa.load(file_name)
            print("Data Length:", len(X))
            stft = np.abs(librosa.stft(X))
            mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)
            chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
            mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
            contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
            tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T,axis=0)
            print("Features Extracted")
            return mfccs,chroma,mel,contrast,tonnetz
        except Exception as e:
            print("Exception in extract_feature ", str(e))
