from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from api import views

# API endpoints
urlpatterns = format_suffix_patterns([
    url(r'^request/$', views.AedAPIProcessingRequest.as_view(), name='aed-request'),
    url(r'^classify/$', views.AedAPIClassify.as_view(), name='aed-classify'),

])
