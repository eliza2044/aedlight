import sys
import os
import wave
import contextlib
import os,binascii
import os,os.path
import shutil
import audioop
import soundfile as sf
import wavio, subprocess,librosa
import numpy as np
import math


from django.conf import settings
from api.ClassificationAlgorithms import ClassificationAlgorithms
from sklearn.model_selection import cross_validate
from sklearn.model_selection import RepeatedKFold
from bson import json_util
from bson.json_util import dumps
from datetime import datetime

import json, uuid

from pymongo import MongoClient
client = MongoClient('db', 27017)
db = client['aed']
datasetCollection=db['datasets']
datasetCol=db.datasetCollection


class trainmodel():

    def train(folder,outrate,samplewidth):
        print("Folder: ",folder,"\nOutrate: ",outrate,"\nSamplewidth: ",samplewidth)

        try:
            outchannels=1
            if len(folder):

                #make sure the directory has been entered correctly

                if not folder.endswith('/'):
                    folder=folder+'/'
                    #print("Added slash at the end of the path")

                audiolist=trainmodel.extractWAVfiles(folder)
                print("audiolist/n",audiolist)

                audioInfo=[]
                for audio in audiolist:
                    audioInfo.append(trainmodel.getWAVinfo(audio))

                #print(audioInfo)
                randhex=binascii.b2a_hex(os.urandom(4))
                curfilePath = os.path.abspath(__file__)
                curDir = os.path.abspath(os.path.join(curfilePath,os.pardir)) # this will return current directory in which python file resides.
                #parentDir = os.path.abspath(os.path.join(curDir,os.pardir)) # this will return parent directory
                dst=curDir+"/"+randhex.strip().decode('ascii')
                #print(dst)

                if not os.path.exists(dst):
                    os.makedirs(dst)
                    print("New Directory Created")

                idx=0
                compatible_audio=[]
                for audio in audiolist:
                    if (samplewidth==audioInfo[idx][1]):
                        trainmodel.downsampleWav(audio, dst, audioInfo[idx][2], outrate, audioInfo[idx][0], outchannels,audioInfo[idx][1],audioInfo[idx][3])
                        idx=idx+1
                        compatible_audio.append(audio)
                    else:
                        print("Sample Width mismatch: ",samplewidth," and ",audioInfo[idx][1])
                audiolist=compatible_audio
                
                trainmodel.generateDataset(audiolist,dst,outrate,samplewidth,"output.wav")

            else:
                print("No args provided")
        except Exception as exp:
            print("Exception in train", str(exp))
    

    def tell_class(y_pred):
        prediction=[]
        for sample in y_pred:
            if sample[0]>sample[1]:
                prediction.append(0)
            else:
                prediction.append(1)
        # print("Prediction is ",prediction)
        y_pred=prediction  
        return y_pred      

    

    def process_dataset(list_of_audios,outrate, clabel):
        try:
            temp_path=settings.PROJECT_ROOT+"/tempdir"
            if not os.path.exists(temp_path):
                os.mkdir(temp_path)

            if not temp_path.endswith('/'):
                temp_path=temp_path+'/'
        except Exception as exp:
            print("Exception in TrainPanelRequest while generating paths", str(exp))
        features, labels = np.empty((0,193)), np.empty(0)
        counter=1

        
        for audio_file in list_of_audios:
            from datetime import datetime
            print(str(datetime.now()), " ## ", counter," out of ", len(list_of_audios), "@ ",audio_file," @")
            # print("============================================================================")
            temp_file=audio_file
            try:
                #Extract features from each file
                mfccs,chroma,mel,contrast,tonnetz=trainmodel.extract_feature(temp_file)
                # print("Individual features extracted")

                ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
                features = np.vstack([features,ext_features])
                labels=np.append(labels,clabel)
            except Exception as exp:

                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print("Exception in TrainPanelRequest while extracting features", str(exp), exc_type, fname, exc_tb.tb_lineno)
            counter+=1
        return features, labels
    def process_prediction_dataset(list_of_audios,outrate):
        try:
            temp_path=settings.PROJECT_ROOT+"/tempdir"
            if not os.path.exists(temp_path):
                os.mkdir(temp_path)

            if not temp_path.endswith('/'):
                temp_path=temp_path+'/'
        except Exception as exp:
            print("Exception in trainmodel.process_prediction_dataset while generating paths", str(exp))

        features, labels = np.empty((0,193)), np.empty(0)
        counter=1
        for audio_file in list_of_audios:
            try:
                k = audio_file.rfind("/")

                temp_file=temp_path+audio_file[k:]
                k = temp_file.rfind(".")
                temp_file = temp_file[:k] + ".wav"

                print("The audio files are getting converted")

                if os.path.exists(temp_file):
                    print("Removing file 388 ",temp_file)
                    os.remove(temp_file)
                subprocess.run(['ffmpeg','-y','-loglevel','quiet', '-i', audio_file,'-ar',str(outrate),'-ac','1',temp_file], shell=False)
            except Exception as exp:
                print("Exception in TrainPanelRequest while homogenising audio files", str(exp))

            try:
                #Extract features from each file
                mfccs,chroma,mel,contrast,tonnetz=trainmodel.extract_feature(temp_file)
                print("Individual features extracted")

                ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
                features = np.vstack([features,ext_features])

            except Exception as exp:
                print("Exception in TrainPanelRequest while extracting features", str(exp))

            print("Removing file 406 ",temp_file)
            os.remove(temp_file)
            counter+=1
        return features

    def extract_feature(file_name):
        X, sample_rate = librosa.load(file_name)
        # print("Data Length:", len(X))
        stft = np.abs(librosa.stft(X))
        mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T,axis=0)
        chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
        mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
        contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
        tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T,axis=0)
        return mfccs,chroma,mel,contrast,tonnetz

    def one_hot_encode(labels):
        n_labels = len(labels)
        print("n_labels ",n_labels)
        n_unique_labels = len(np.unique(labels))
        print("n_unique_labels ",n_unique_labels)
        one_hot_encode = np.zeros((n_labels,n_unique_labels))
        print("one_hot_encode ",one_hot_encode)
        one_hot_encode[np.arange(n_labels), labels] = 1
        return one_hot_encode

    def extractWAVfiles(directory):
        try:
            walk_dir = directory
            # print('Extracting files from ' + walk_dir)
            audio=[]
            for root, subdirs, files in os.walk(walk_dir):
                try:
                    print("files are:", files)
                    for filename in files:
                        ext=filename.split(".")[-1]

                        if (ext.lower()!="wav" or ext.lower()!="wave"):
                            # print("File is not a WAVE",filename[-4:].lower(), filename[-5:].lower())
                            k=filename.rfind(".")
                            wavitem=filename[:k]+".wav"
                            print("Converting ",os.path.join(directory, filename), " and generating ", os.path.join(directory, wavitem))
                            subprocess.run(['ffmpeg','-y','-loglevel','quiet', '-i', os.path.join(directory, filename),'-ar','16000','-ac','1',os.path.join(directory, wavitem)], shell=False)
                            file_path = os.path.join(directory, wavitem)
                            if os.path.join(directory, filename)!=os.path.join(directory, wavitem):
                                if os.path.exists(os.path.join(directory, filename)):
                                    os.remove(os.path.join(directory, filename))
                        else:
                            file_path = os.path.join(directory, filename)
                        audio.append(file_path)
                except Exception as e:
                    print("Exception thrown: ",str(e))
            return audio

        except Exception as exc:
            print("Something went wrong "+str(exc))
            return []
    def extractFilesTypes(directory,exte, id=None):
        try:
            print("In extract files types, the directory is: ", directory)
            walk_dir = directory
            audio=[]
            # allfiles=[]
            exte="."+exte
            print("files are:", os.listdir())
            # for root, subdirs, files in os.walk(walk_dir):
            for filename in os.listdir():
                try:
                    p=filename.rfind(".")
                    if p!=-1:
                        ext=filename[p:]
                        # allfiles.append(filename)
                        # print("Filename is # ",filename," # Ext is: ## ",ext, " ## and exte: ",exte, "p is ", p, " length is ", len(filename))
                        if (ext.lower()==exte):
                            if id==None:
                                audio.append(filename)
                            elif id==filename[:p]:
                                audio.append(filename)

                        
                except Exception as e:
                    print("Exception thrown: ",str(e))
                    return []
            return audio
        except Exception as exc:
            print("Something went wrong "+str(exc))
            return []
    def extractFiles(audio,directory, name):
        try:
            walk_dir = directory
            # print('Extracting files from ' + walk_dir)
            print("Extracting files with name ", name, " in directory ", directory)
            for root, subdirs, files in os.walk(walk_dir):
                try:
                    import ntpath
                    name=ntpath.basename(name)
                    k=name.rfind(".")
                    print(files)
                    for filename in files:
                        p=filename.rfind(".")
                        notextfile=filename[:p]
                        noextname=name[:k]
                        print("notextfile ",notextfile,"No ext name is ", noextname)
                        if (not notextfile.find(noextname)==-1):
                            print("not notextfile.find(noextname)==-1")
                            file_path = os.path.join(directory, filename)
                            audio.append(file_path)

                except Exception as e:
                    print("Exception thrown: ",str(e))
            # try:
            #     audio.sorted()
            # except Exception as ex:
            #     exc_type, exc_obj, exc_tb = sys.exc_info()
            #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            #     print("Exception ", str(ex),exc_type, fname, exc_tb.tb_lineno)
            return audio #.sort()

        except Exception as ex:
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print("Exception ", str(ex),exc_type, fname, exc_tb.tb_lineno)
            return []
    def sort_alpha(audiolist):
        try:
            # noext=[]
            increments=[]
            for item in audiolist:
                import ntpath
                item=ntpath.basename(item)
                p=item.rfind(".")
                notextfile=item[:p]
                name,incre=notextfile.split('-')
                increments.append(int(incre))
            increments=sorted(increments,reverse=False )
            audiolist=[]
            for item in increments:
                audiolist.append(name+'-'+str(item)+".wav")
            # for item in noext:
            #     audiolist.append(item+".wav")
            # print("Printing noext in sort_alpha: ",audiolist)
        
        except Exception as exc:
            pass
        return audiolist
    def getWAVinfo(fname):
        with contextlib.closing(wave.open(fname,'r')) as f:
            '''
            frames = f.getnframes()
            rate = f.getframerate()
            duration = frames / float(rate)
            channels= f.getnchannels()
            depth=f.getsampwidth()
            print(fname,duration, rate, frames,channels, depth)
            '''
            #(nchannels, sampwidth, framerate, nframes, comptype, compname)
            #print(fname,"\n",f.getparams())
            return f.getparams()

    def downsampleWav(src, dst, inrate=44100, outrate=16000, inchannels=2, outchannels=1,sampwidth=2,nframes=0):
        if not os.path.exists(src):
            print('Source not found!',src)
            return False

        if not os.path.exists(os.path.dirname(dst)):
            os.makedirs(os.path.dirname(dst))

        try:
            if (sampwidth==3):
                print("Changing width of audio file")
                changeWidth(src)

            s_read = wave.open(src, 'r')
            dstfile=os.path.basename(src)

            dstfile=dst+"/"+dstfile
            s_write = wave.open(dstfile, 'wb')
            s_write.setparams((outchannels, sampwidth, outrate, 0, 'NONE', 'not compressed'))
        except:
            print('Failed to open files!')
            return False

        n_frames = s_read.getnframes()
        data = s_read.readframes(n_frames)


        if (inrate!=outrate):
            try:
                converted = audioop.ratecv(data, sampwidth, inchannels, inrate, outrate, None)
                if outchannels == 1:
                    converted = audioop.tomono(converted[0], sampwidth, 1, 0)

            except Exception as exp:
                print('Failed to downsample wav', sys.exc_info()[0])
                print("Conversion error({0})".format(str(exp)))
                #shutil.rmtree(os.path.dirname(dst))
                return False
        else:
            converted=data

        try:
            #                 (nchannels, sampwidth, framerate, nframes, comptype, compname)
            #s_write.setparams((outchannels, 2, outrate, 0, 'NONE', 'Uncompressed'))
            s_write.setparams((outchannels, sampwidth, outrate, nframes, 'NONE', 'Uncompressed'))
            s_write.writeframes(converted)
        except Exception as exp:
            print("Failed to write wav  {0}".format(str(exp)))
            print(outchannels, sampwidth, outrate, nframes, 'NONE', 'Uncompressed')
            return False

        try:
            s_read.close()
            s_write.close()
        except:
            print('Failed to close wav files')
            return False

        return True

    def changeWidth(src):
        try:
            data, samplerate = sf.read(src)
            sf.write(src, data, samplerate, subtype='PCM_16')
            print("WAV info\n",trainmodel.getWAVinfo(src))
        except Exception as e:
            print("Error while converting width {0})".format(str(e)))

    def generateDataset(audiolist,dst,rate,sampwidth, filename="output.wav"):
        # output=dst+'/'+filename
        print("Destination of concatenated audio", filename)
        print("About to join audio files in",dst)
        data= []
        for infile in audiolist:

            w = wave.open(infile, 'rb')
            print(infile, w.getnframes())
            data.append( [w.getparams(), w.readframes(w.getnframes())] )
            w.close()

        print("About to write file in directory",dst)
        output = wave.open(filename, 'wb')
        output.setnchannels(1)
        output.setframerate(rate)
        print("samplewidth",sampwidth)
        output.setsampwidth(sampwidth)
        print("data", len(data))

        output.close()
        print("Successfully written output file")
        
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
