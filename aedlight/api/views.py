from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework import status
from rest_framework.settings import api_settings
# from bson import json_util

# from bson.json_util import dumps
from pprint import pprint
import json
import uuid
import numpy as np
import pickle, collections
from api.ClassificationAlgorithms import ClassificationAlgorithms
from api.trainmodel import trainmodel

#Response and request header
header = {'Content-Type' : 'application/json'}

class AedAPIProcessingRequest(APIView):
    """Post Request"""

    def post(self, request, format=None):
        print("In AedAPIProcessingRequest POST")
        payload = None
        payload=json.loads(json.dumps(request.data))
        print("Request", json.dumps(payload))

        # TODO: check that all required input fields were provided


        try:
            return Response({
                "status": "success",
                "message": "Job Recieved",
                }, status = status.HTTP_202_ACCEPTED,headers=header)

        except Exception as e:
            return Response({"detail" : "Failed to communicate: "+str(e)},
                            status = status.HTTP_400_BAD_REQUEST,
                            headers=header)



class AedAPIClassify(APIView):
    """IN GET LIST"""
    #SecurityLayer
    def get(self, request, format=None):
        print("In AedAPIClassify GET")
        try:
            return Response({
                "status": "success",
                "message": "Service is running",
                }, headers=header)

        except Exception as e:
            return Response({"detail" : "Failed to communicate: "+str(e)},
                            status = status.HTTP_400_BAD_REQUEST,
                            headers=header)

    """Post Request"""

    def post(self, request, format=None):
        print("In AedAPIClassify POST")
        payload = None

        payload=json.loads(json.dumps(request.data))
        print("Request")
        print(json.dumps(payload, sort_keys=True, indent=4))
        features=np.empty((0, 193))
        try:         
            import requests
            import datetime
            aedResult=[]
            import os.path
            # curfilePath = os.path.abspath(__file__)
            
            # curDir = os.path.abspath(os.path.join(curfilePath,os.pardir))
            # curDir = os.path.abspath(os.path.join(curDir,os.pardir))
            
            import os
            import ntpath
            file=payload['file']
            exists = os.path.isfile(file)
            if not exists:
                return Response({"reason" : "Operation failed. The file "+ntpath.basename(file)+" does not exist.", "operation":"REQ_FAILED"},
                            status = status.HTTP_400_BAD_REQUEST,
                            headers=header)

            curDir="/ASGARD_DATA"

            try:
                # temp_path=os.path.abspath(os.path.join(curDir,os.pardir))
                
                # /code/tempdir
                # temp_path=temp_path+"/tempdir"
                temp_path=payload['outfolder'] #+"/tempdir"
                # print("1.temp_path: ", temp_path)
                print("2.temp_path: ", temp_path)
                if not os.path.exists(temp_path):
                    os.mkdir(temp_path)

                if not temp_path.endswith('/'):
                    temp_path=temp_path+'/'
            except Exception as exp:
                print("Exception in TrainPanelRequest while generating paths", str(exp))
            
            
            print("ntpath.basename(file): ", ntpath.basename(file))
            dst=temp_path+ntpath.basename(file)
            counter=1
            features = np.empty((0,193))
            fileresult=[]
            audiolist=[]
            toreturn=[]
            print("/*"*30)
           
            
            # print("Filename ",ntpath.basename(file))
            # print("The file is: ",file)
            # print("The dst is: ",dst)

            print("Filename ",ntpath.basename(file))
            print("The input folder is: ",file)
            print("The output folder is: ",dst)


            print("*/"*30)
            try:
                import wave, os, subprocess
                k=dst.rfind(".")
            except Exception as ex:
                print("Error while constructing wav filename", str(ex))
            try:
                print("About to convert file ", file)
                import wave, os, subprocess
                if  not os.path.exists(file):
                    return Response({"detail" : "The file does not exist" +file},
                    status = status.HTTP_400_BAD_REQUEST,
                    headers=header)

                noext=file.rfind('.')
                print("File ", file )
                print("File No EXT is ", file[noext+1:] )
                converted=False
                if file[noext:]!='wav':
                    converted=True
                    noext=file[:noext]
                    noext=noext+".wav"
                    print("COnverting file ", file," to file ", noext)
                    # subprocess.run(['ffmpeg','-y','-loglevel','quiet', '-i', file,'-vn', '-acodec','pcm','s16le', '-ar','16000','-ac', '1', noext])
                    subprocess.run(['ffmpeg','-y','-loglevel','quiet', '-i', file,'-ar','16000','-ac','1',noext], shell=False)
                    # subprocess.run(['ffmpeg','-y', '-loglevel','quiet', '-i', file,'-vn', '-acodec','pcm_u16', '-ar','16000','-ac', '1', noext])
                    file=noext
                    print("The file is now", file, " and dst is ", dst[:k])
                    if os.path.exists(file):
                        print("The file ", file," exists!")

                
                subprocess.run(['ffmpeg','-y', '-i', file, '-f', 'segment', '-segment_time', '1', '-c', 'copy','-ar','16000','-ac','1',dst[:k]+'-%d.wav'], shell=False)
                if os.path.exists(file) and converted:
                    os.remove(file)
                    print(str(datetime.datetime.now()), " ## ","Removing file: ",file)
                print("After FFMPEG", dst)

                audiolist=trainmodel.extractFiles(audiolist, payload['outfolder'],payload['file'])
                print("Audiolist: ", audiolist)

                if not audiolist:
                    return Response({"reason" : "Operation failed. The file "+ntpath.basename(file)+" is too short in duration", "operation":"REQ_FAILED"},
                            status = status.HTTP_400_BAD_REQUEST,
                            headers=header)

                audiolist=trainmodel.sort_alpha(audiolist)
                # oldfile=item
                try:
                    # temp_file=item
                    row_count=1
                    segments=[]
                    
                    for row in audiolist:
                        row=payload['outfolder']+'/'+row

                        print(str(datetime.datetime.now()), " ## ","Extracting features ", row_count, " out of ", len(audiolist)," filename ", row)
                        try:
                            #Extract features from each file audiolist
                            mfccs,chroma,mel,contrast,tonnetz=trainmodel.extract_feature(row)
                            ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
                            features = np.vstack([features,ext_features])

                            segments.append({"segmentName":row,"features":json.dumps(ext_features, cls=NumpyEncoder) })
                        except Exception as ex:
                            import sys
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                            print("Exception ", str(ex),exc_type, fname, exc_tb.tb_lineno)
                        row_count+=1
                    print("=========================================================")
                    fileresult.append({'filename':file,'segments':segments})
                    counter+=1
                except Exception as exp:
                    print(str(datetime.datetime.now()), " ## ","Exception while extracting features from",row," @ ", str(exp)," @ ")
                try:
                    for row in audiolist:
                        print("about to remove file ",row)
                        if os.path.exists(row) and not file==row:
                            os.remove(row)
                            print(str(datetime.datetime.now()), " ## ","Removing file: ",row)
                except Exception as ex:
                    print("Exception while removing files", str(ex))
                    # print("temp_file", temp_file, "oldfile", oldfile)

            except Exception as ex:
                import sys
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print("Exception ", str(ex),exc_type, fname, exc_tb.tb_lineno)

            print(str(datetime.datetime.now()), " ## ","Audio list from folder", "##", dst," @@ ")
            # print("fileresult: ", fileresult)
            print(len(fileresult), " items in fileresult")
            print(fileresult, " items in fileresult")
            toReturn=[]
            ovResult=[]

            curfilePath = os.path.abspath(__file__)
            curDir = os.path.abspath(os.path.join(curfilePath, os.pardir))
            curDir = os.path.abspath(os.path.join(curDir, os.pardir))
            curDir = os.path.abspath(os.path.join(curDir, os.pardir))
            print("********************************************************")
            curDir=curDir+"/models/"+payload['trainingID']+".csv"
            print("curDir", curDir)
            for item in fileresult:
                print(type(item))
                # print(json.dumps(item))
                count={}
                print(len(item['segments']), " files in item")
                totalBlocks=len(item['segments'])
                for segment in item['segments']:
                    print(segment['segmentName'])
                    sample=np.array(json.loads(segment['features']))
                    sample=sample.reshape(1, -1)
                    try:
                        import csv
                        import numpy
                        reader=csv.reader(open(curDir,"r"),delimiter=',')
                        x=list(reader)
                        result=numpy.array(x).astype('float')
                        result=result.astype(int)
                        result=result.reshape(1,-1)
                        result=result[0]
                        print("result is: ", result)

                        if len(result)>0:
                            sample=sample[:, result]
                    except Exception as excep:
                        print("ERROR::::: while retaining features: ",str(excep))
                    result=ClassificationAlgorithms.classify(payload['trainingID'],sample,payload['event'],segment)
                    print("Result: ",result)
                    
                    
                    segment['result']=result['result']
                    count[payload['event']]=collections.Counter(result)
                    detection_dict={}
                    detection_dict=collections.Counter(result)
                    import math
                    eventsresults=[]

                    try:
                        maxvalue=0
                        for el in detection_dict['result']:
                            if maxvalue<el['probability']:
                                maxvalue=el['probability']
                                maxkey=el['event']
                        description=maxkey+" result with "+ "{0:.2f}".format(maxvalue*100)+"% confidence"
                        keydata={'segment':segment['segmentName'],'event':maxkey,'description':description,'score':math.ceil(maxvalue*100)}
                        toReturn.append(keydata)
                    except Exception as e:
                        print("Exception thrown while shaping response ", str(e))
                print("+"*10,"-"*10,"+"*10,"-"*10)

                maliciousBlocks=0
                avPosDet=0
                avNegDet=0
                
                for segment in toReturn:
                    if segment['event'] != "False" and segment['score']>50:
                        maliciousBlocks+=1
                        avPosDet+=segment['score']
                    elif segment['event'] == "False" and segment['score']>50:
                        avNegDet+=segment['score']
                print("+-*/"*20)
                print("avPosDet: ", avPosDet,"avNegDet: ", avNegDet, "totalBlocks", totalBlocks, "maliciousBlocks", maliciousBlocks)
                print("+-*/"*20)

                file_Result={}
                if totalBlocks!=0:
                    if maliciousBlocks!=0:
                        avPosDet=avPosDet/maliciousBlocks
                        print("Normalised avPosDet: ", avPosDet)

                    if avNegDet!=0:
                    # if totalBlocks!=0 and totalBlocks!=maliciousBlocks:
                        avNegDet=avNegDet/(totalBlocks- maliciousBlocks)
                    else:
                        avNegDet=0

                    
                    file_Result['posBlocks']=maliciousBlocks
                    if maliciousBlocks!=0:
                        file_Result['posBlocksPercentage']=maliciousBlocks/totalBlocks*100
                    else:
                        file_Result['posBlocksPercentage']=0

                    file_Result['negBlocks']=totalBlocks-maliciousBlocks
                    print("totalBlocks: ", totalBlocks, " maliciousBlocks: ", maliciousBlocks)
                    file_Result['negBlocksPercentage']=(totalBlocks-maliciousBlocks)/totalBlocks*100
                    file_Result['totalBlocks']=totalBlocks
                    file_Result['avPosEventDetection']=avPosDet
                    file_Result['avNegEventDetection']=avNegDet    
                    file_Result['detailedResults']=toReturn
                    
                else:
                    file_Result['description']="No data could be extracted, the file is too short"
                file_Result['file']=ntpath.basename(payload['file'])
                file_Result['event']=payload['event']
                ovResult.append(file_Result)
        except Exception as e:
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print("Exception ", str(e),exc_type, fname, exc_tb.tb_lineno)
            return Response({"detail" : "Error in AedAPIClassify Request" +str(e)},
                            status = status.HTTP_404_NOT_FOUND,
                            headers=header)

        from time import gmtime, strftime
        if not payload['outfolder'].endswith('//'):
            output=payload['outfolder']+'//'
        else:
            output=payload['outfolder']

        output=payload['outfolder']+'/'+strftime("%Y-%m-%d_%H%M%S", gmtime())+payload['trainingID']
        text_file= open(output,"w+")
        text_file.write(json.dumps(ovResult))
        text_file.close()
        return Response({
                    "status": "success",
                    "message": "Classification",
                    "result": ovResult,
                    }, headers=header)
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
