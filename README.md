# aedlight #

*This README explains how to set up the docker container. For information about how to use once deployed*

The aedlight tool is a lightweight version of the acoustic event detection suite that provides the service of classifying audio files given the classification model. The tool works as a models placeholder for models trained with the AED tool.
THe tool may accept one or more files for classification at a time, files might be of any type supported by the FFMPEG library.


## Option A: Installation (Dockerised version) ##

The installation of the aedlight tool involves the retrieval of source code for the setup of the tool.

	Prerequisites: Ensure that docker  and docker-compose are aleady installed on the host machine.

### Step 1: Clone the main repository
Clone the current repository and navigate into the demonstrator-wp6-t2_acoustic_event_detection direcotry
```shell
	git clone https://eliza2044@bitbucket.org/eliza2044/aedlight.git
	cd aedlight
```

### Step 2: Build the Docker Contrainer

Build the docker container by running the following commands:

```shell
	sudo docker-compose build
```
Once it builds then run

```shell
	sudo docker-compose up
```
## Configuration ##

* Trained models should be placed in the /code/models folder
* The folder parameter involves the path to the files to be processed and files is the array of filenames in the folder path to be analysed.

## Operation ##

Call the web service at: *POST call: http://0.0.0.0:8001/api/classify/*

Input should be of the following format:

```
{  
   "event":"gunshot",
   "file":[  
      "7061.wav",
      "7062.wav"
   ],
   "folder":"/code/inputfiles",
   "timestamp":"2018-10-20T06:59:45.177562",
   "trainingID":"c321f19372bf443f9ec2df57a0c42077"
}
```

Output is of the following format

```
{
    "status": "success",
    "result": {
        "result": [
            {
                "probability": 1,
                "event": "False",
                "file": "7061.wav"
            },
            {
                "probability": 1.0058317404743825e-99,
                "event": "gunshot",
                "file": "7061.wav"
            },
            {
                "probability": 1,
                "event": "False",
                "file": "7062.wav"
            },
            {
                "probability": 4.297937564815067e-25,
                "event": "gunshot",
                "file": "7062.wav"
            }
        ]
    },
    "message": "Classification"
}
```

## Option B: Setup the tool for Production

This section explains how to run the packaged tool on production servers using [Docker](https://www.docker.com).  

``` bash
# Login to docker registry
sudo docker login docker-registry.darkwebmonitor.eu

# Pull docker image of tool
sudo docker pull docker-registry.darkwebmonitor.eu/aedlight_web:20181021r1stable
```

### Who do I talk to? ###
This repository is maintained by Liza Charalambous from ADITESS
* Mattermost: lc-aditess
* Skype: charalambous.liza
* Email: lc@aditess.com
